---
title: "Types of Checklist Questions"
permalink: /guides/checklists/types-of-checklist-questions/
---

{% include base_path %}
{% include toc %}

There are 4 types of questions a subscriber can add to a checklist in Service Automation:
* [Acknowledgement](#acknowledgement)
* [Selector](#selector)
* [Text Entry](#text-entry)
* [Numeric Input](#numeric-input)

Refer to the table below to find out what values represent these question types in the API.

Question type in Service Automation | `QuestionType` value in the API
-----|-----
Acknowledgement | 0
Selector | 1
Text Entry | 2
Numeric Input | 4

Let's take a closer look at the questions included in the _Outdoor Lighting Checklist_ we [retrieved]({{base_path}}/guides/checklists/find-and-filter-checklists/#retrieve-a-specific-checklist) earlier and compare how each question type appears in the ServiceChannel Provider mobile app and in the API. For each question type, you can see a question object and a table highlighting the most important parameters. Pay attention to this data when sending API requests.

# Acknowledgement

An **Acknowledgement** question represents an on/off toggle prompting a technician to give a positive or negative response to a question.

![Acknowledgement question in the ServiceChannel Provider mobile app]({{base_path}}/assets/images/guides/checklists/checklist-SCP-acknowledgement.jpg "Acknowledgement question in the ServiceChannel Provider mobile app")

In our checklist, adding an attachment to this question is required.

`Question` | `Id` | `QuestionType` | `PictureFlag`
-----|-----
Are you wearing protective gear? | 3834 | 0 — Acknowledgement | 2 — Uploading an attachment is mandatory.

**Question object**
{: .collapsible}

``` json
{
   "Id": 3834,
   "CheckListId": 496,
   "Question": "Are you wearing protective gear?",
   "ShortDescription": null,
   "AnswerRequired": false,
   "PictureFlag": 2,
   "Sequence": 0,
   "QuestionType": 0,
   "CheckListQuestionOptions": [],
   "AnswerCanPersist": true,
   "CheckListTimingType": 1
}
```
{: .colcontent}

# Selector

A **Selector** question provides multiple answer options to choose from.

![Selector question in the ServiceChannel Provider mobile app]({{base_path}}/assets/images/guides/checklists/checklist-SCP-selector.jpg "Selector question in the ServiceChannel Provider mobile app")

In our example, a user should choose one of the two answer options. If a technician picks the second answer, they should also enter a comment because a text field is enabled for this option. Since answering this question is required, the comment is also mandatory.

_**Note:**_ The `IsConditionalFollowUp` parameter in the API corresponds to the **User Input Response** checkbox in the checklist overlay in Service Automation.
{: .notice--primary}

`Question` | `Id` | `QuestionType` | `PictureFlag`
-----|-----
Are all entrance lights functioning properly? | 3835 | 1 — Selector | 0 — Adding an attachment is optional.

`CheckListQuestionOptions` | `IsConditionalFollowUp`
-----|-----
4856 — *Yes* | false — A user comment for an answer option is not enabled.
4857 — *No (please explain)* | true — A user can enter a comment for an answer option.

**Question object**
{: .collapsible}

``` json
{
   "Id": 3835,
   "CheckListId": 496,
   "Question": "Are all entrance lights functioning properly?",
   "ShortDescription": null,
   "AnswerRequired": true,
   "PictureFlag": 0,
   "Sequence": 0,
   "QuestionType": 1,
   "CheckListQuestionOptions": [
      {
         "Id": 4856,
         "Answer": "Yes",
         "Sequence": 0,
         "IsConditionalFollowUp": false
      },
      {
         "Id": 4857,
         "Answer": "No (please explain)",
         "Sequence": 0,
         "IsConditionalFollowUp": true
      }
   ],
   "AnswerCanPersist": false,
   "CheckListTimingType": 1
}
```
{: .colcontent}

# Text Entry

A **Text Entry** question allows a technician to enter a free text answer.

![Text Entry question in the ServiceChannel Provider mobile app]({{base_path}}/assets/images/guides/checklists/checklist-SCP-text-entry.jpg "Text Entry question in the ServiceChannel Provider mobile app")

Note that there is no possibility to add an attachment to the response as per the question settings.

`Question` | `Id` | `QuestionType` | `PictureFlag`
-----|-----
Please specify if you see any dark areas in the parking lot that could be a safety risk. | 3836 | 2 — Text Entry | 1 — An attachment is not required.

**Question object**
{: .collapsible}

``` json
{
   "Id": 3836,
   "CheckListId": 496,
   "Question": "Please specify if you see any dark areas in the parking lot that could be a safety risk.",
   "ShortDescription": null,
   "AnswerRequired": false,
   "PictureFlag": 1,
   "Sequence": 0,
   "QuestionType": 2,
   "CheckListQuestionOptions": [],
   "AnswerCanPersist": false,
   "CheckListTimingType": 1
}
```
{: .colcontent}

# Numeric Input

A **Numeric Input** question prompts a user to enter a number as a response.

![Numeric Input question in the ServiceChannel Provider mobile app]({{base_path}}/assets/images/guides/checklists/checklist-SCP-numeric-input.jpg "Numeric Input question in the ServiceChannel Provider mobile app")

Just like in the [second question](#selector), a user can add an attachment to the response, but it's not required.

`Question` | `Id` | `QuestionType` | `PictureFlag`
-----|-----
How many lights need repair or replacement? | 3837 | 4 — Numeric Input | 0 — Adding an attachment is optional.

**Question object**
{: .collapsible}

``` json
{
   "Id": 3837,
   "CheckListId": 496,
   "Question": "How many lights need repair or replacement?",
   "ShortDescription": null,
   "AnswerRequired": true,
   "PictureFlag": 0,
   "Sequence": 0,
   "QuestionType": 4,
   "CheckListQuestionOptions": [],
   "AnswerCanPersist": false,
   "CheckListTimingType": 1
}
```
{: .colcontent}

{% include collapsible.html %}