---
title: "Submit a Checklist Response"
permalink: /guides/checklists/submit-a-checklist-response/
---

{% include base_path %}
{% include toc %}

Typically, technicians [complete checklists](https://servicechannel.atlassian.net/wiki/spaces/SCUCP/pages/655327717/Work+Orders+with+Attached+Checklists){:target="_blank"} in the ServiceChannel Provider mobile app when performing work on service requests for which checklists are triggered. Let's try to do this via the API.

# Step 1. Get the Required Data

First, make sure the **work order** you are submitting checklist responses for is opened for a trade and category for which a checklist is configured. Keep in mind that checklists are created for unique combinations of trade and category. If the feature that allows enabling checklists for specific locations is turned on for a subscriber, double-check that the service request is created for the right location.

Second, obtain the **IDs of the provider and technician** on whose behalf you want to submit responses.

ProviderID
{: .collapsible}

<div>
<p>Provider ID is a unique numeric identifier of the provider in the ServiceChannel system.</p>
<p>Subscribers can use the following methods to get the <code>Id</code> of the required provider.</p>
<div class="notice--info">
<strong><a href="{{base_path}}/swagger/ui/index?version=3#/Providers/GETv3_providers_getbytrade_trade__trade_" target="_blank">GET /providers/getbytrade</a></strong>
</div>
<div class="notice--info">
<strong><a href="{{base_path}}/swagger/ui/index?version=3#/Providers/Providers_GetRecent" target="_blank">GET /odata/locations({locationId})/providers/Service.GetRecent(days={days})</a></strong>
</div>
<p>Provider users who have access to Provider Automation can see the company ID in the lower-right corner of the home page.</p>
</div>
{: .colcontent}

Third, save the **IDs of checklist questions**. You can find them by sending the request to [get checklists]({{base_path}}/guides/checklists/find-and-filter-checklists/).

# Step 2. Answer Checklist Questions

Let’s complete the *Outdoor Lighting Checklist* we [retrieved]({{base_path}}/guides/checklists/find-and-filter-checklists/#retrieve-a-specific-checklist) earlier. We will submit checklist responses for work order #180088351 belonging to the *Electrical* trade and *Repair* category on behalf of a technician Adam Dover who hasn't yet performed any work on the request.

**POST /checklists/CheckListResponse**
{: .notice--success}

Header | Value
-----|-----
Content-Type | multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

Parameter | Parameter type | Description
-----|-----|-----
CheckListResponseRequest | formData | Request to submit a response to a checklist question.<br/>See below for details.
file | formData | Attachment that supports your response to a checklist question. Usually, technicians add a picture.

**`CheckListResponseRequest` object**

Parameter | Parameter type | Required | Description
-----|-----|-----|-----
CheckListQuestionID | Integer | Required | ID of the checklist question you are submitting a response for.
CheckListQuestionOptionID | Integer | Required for **Selector** questions | ID of the answer option configured for a **Selector** question.
ProviderID | Integer | Required | ID of the provider on whose behalf you are submitting a response.
TechnicianID | Integer | Required | ID of the technician on whose behalf you are submitting a response.
Answer | String | Required for all question types except **Selector** | Answer to a checklist question of the following types:<br/>&#8195;&#8226; **Acknowledgement**. Valid values: on, off, 1, 0, true, false, yes, no.<br/>&#8195;&#8226; **Text Entry**. Enter a text answer. The maximum length is 500 characters.<br/>&#8195;&#8226; **Numeric Input**. Submit a number.
FreeTextAnswer | String | See *Description* | Comment to an answer option with the `IsConditionalFollowUp` attribute. Answer options are configured for **Selector** questions.
EntityID | Integer | Required | Unique ID of the work order for which you are creating a response to a checklist question.
Attempt | Integer | Required | Order of a technician’s on-site visit you are submitting a checklist response for.

Note that `CheckListQuestionID`, `ProviderID`, `TechnicianID`, `EntityID`, and `Attempt` are required parameters for all question types.

## Acknowledgement Question

`Question` | `Id` | `QuestionType` | `PictureFlag`
-----|-----
Are you wearing protective gear? | 3834 | 0 — Acknowledgement | 2 — Uploading an attachment is mandatory.

Our technician Adam is wearing protective gear on site, so the answer to this question is positive. Passing _on_, _1_, _true_, or _yes_ in `Answer` in the request is equivalent to enabling the toggle for this question in the ServiceChannel Provider mobile app.

Since uploading an attachment is required for this question, we will add a picture to the response.

[<i class="fa fa-play" aria-hidden="true"></i>]({{base_path}}/swagger/ui/index?version=3#/CheckLists/POSTv3_checklists_CheckListResponse){:target="_blank"}{: .btn .btn--primary .btn--small} **Example request for an [Acknowledgement]({{base_path}}/guides/checklists/types-of-checklist-questions/#acknowledgement) question:**

```http
POST https://sb2api.servicechannel.com/v3/checklists/CheckListResponse HTTP/1.1
Authorization: Bearer {access_token}
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

----WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="CheckListResponseRequest"
{
   "CheckListQuestionID": 3834,
   "ProviderID": 2000090505,
   "TechnicianID": 371122,
   "Answer": "on",
   "EntityID": 180088351,
   "Attempt": 1
}
----WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="file"; filename="/C:/protective-gear.jpg"
Content-Type: image/jpeg

(data)
----WebKitFormBoundary7MA4YWxkTrZu0gW
```

**Example response**
{: .collapsible}

``` json
{
   "CheckListResponse": {
      "CheckListQuestionId": 3834,
      "CheckListQuestionOptionId": null,
      "ProviderId": 2000090505,
      "TechnicianId": 371122,
      "Id": 1932333,
      "Answer": "on",
      "FreeTextAnswer": null,
      "EntityId": 180088351,
      "EntityTypeId": 0,
      "Attempt": 1,
      "UpdatedDate": "2021-06-07T09:50:05.893",
      "UpdatedDateDto": "2021-06-07T09:50:05.893-04:00"
   },
   "CheckListResponsePicture": {
      "Id": 114012,
      "CheckListResponseId": 1932333,
      "DocumentName": "protective-gear.jpg",
      "DocumentPath": "#workorders/02619396-ae81-471d-8a78-f80b4d805bad",
      "ThumbnailPath": "#workorders/21417c6f-83b0-4a50-90e9-b372c4a6b0f8",
      "HasThumbnail": true,
      "UpdatedBy": null
   }
}
```
{: .colcontent}

## Selector Question

`Question` | `Id` | `QuestionType` | `PictureFlag`
-----|-----
Are all entrance lights functioning properly? | 3835 | 1 — Selector | 0 — Adding an attachment is optional.

`CheckListQuestionOptions` | `IsConditionalFollowUp`
-----|-----
4856 — *Yes* | false — A user comment for an answer option is not enabled.
4857 — *No (please explain)* | true — A user can enter a comment for an answer option.

When entering the location, Adam noticed that one entrance light was blinking. Since not all lights are functioning properly, we will select *No (please explain)* by passing the answer option number in `CheckListQuestionOptionId`. We should also leave a comment in `FreeTextAnswer` because a text input field is enabled for this option, and answering this checklist question is required.

[<i class="fa fa-play" aria-hidden="true"></i>]({{base_path}}/swagger/ui/index?version=3#/CheckLists/POSTv3_checklists_CheckListResponse){:target="_blank"}{: .btn .btn--primary .btn--small} **Example request for a [Selector]({{base_path}}/guides/checklists/types-of-checklist-questions/#selector) question:**

```http
POST https://sb2api.servicechannel.com/v3/checklists/CheckListResponse HTTP/1.1
Authorization: Bearer {access_token}
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

----WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="CheckListResponseRequest"

{
   "CheckListQuestionID": 3835,
   "CheckListQuestionOptionId": 4857,
   "ProviderID": 2000090505,
   "TechnicianID": 371122,
   "FreeTextAnswer": "One light is blinking",
   "EntityID": 180088351,
   "Attempt": 1
}
----WebKitFormBoundary7MA4YWxkTrZu0gW
```

**Example response**
{: .collapsible}

``` json
{
   "CheckListResponse": {
      "CheckListQuestionId": 3835,
      "CheckListQuestionOptionId": 4857,
      "ProviderId": 2000090505,
      "TechnicianId": 371122,
      "Id": 1932655,
      "Answer": "No (please explain)",
      "FreeTextAnswer": "One light is blinking",
      "EntityId": 180088351,
      "EntityTypeId": 0,
      "Attempt": 1,
      "UpdatedDate": "2021-06-07T10:20:48.073",
      "UpdatedDateDto": "2021-06-07T10:20:48.073-04:00"
   },
   "CheckListResponsePicture": {
      "Id": 0,
      "CheckListResponseId": 0,
      "DocumentName": null,
      "DocumentPath": null,
      "ThumbnailPath": null,
      "HasThumbnail": null,
      "UpdatedBy": null
   }
}
```
{: .colcontent}

## Text Entry Question

`Question` | `Id` | `QuestionType` | `PictureFlag`
-----|-----
Please specify if you see any dark areas in the parking lot that could be a safety risk. | 3836 | 2 — Text Entry | 1 — An attachment is not required.

The location's parking lot is perfectly lit, so the technician enters a comment about that in `Answer`.

[<i class="fa fa-play" aria-hidden="true"></i>]({{base_path}}/swagger/ui/index?version=3#/CheckLists/POSTv3_checklists_CheckListResponse){:target="_blank"}{: .btn .btn--primary .btn--small} **Example request for a [Text Entry]({{base_path}}/guides/checklists/types-of-checklist-questions/#text-entry) question:**

```http
POST https://sb2api.servicechannel.com/v3/checklists/CheckListResponse HTTP/1.1
Authorization: Bearer {access_token}
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

----WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="CheckListResponseRequest"

{
   "CheckListQuestionID": 3836,
   "ProviderID": 2000090505,
   "TechnicianID": 371122,
   "Answer": "No dark areas",
   "EntityID": 180088351,
   "Attempt": 1
}
----WebKitFormBoundary7MA4YWxkTrZu0gW
```

**Example response**
{: .collapsible}

``` json
{
   "CheckListResponse": {
      "CheckListQuestionId": 3836,
      "CheckListQuestionOptionId": null,
      "ProviderId": 2000090505,
      "TechnicianId": 371122,
      "Id": 1932663,
      "Answer": "no dark areas",
      "FreeTextAnswer": null,
      "EntityId": 180088351,
      "EntityTypeId": 0,
      "Attempt": 1,
      "UpdatedDate": "2021-06-07T10:21:46.94",
      "UpdatedDateDto": "2021-06-07T10:21:46.94-04:00"
   },
   "CheckListResponsePicture": {
      "Id": 0,
      "CheckListResponseId": 0,
      "DocumentName": null,
      "DocumentPath": null,
      "ThumbnailPath": null,
      "HasThumbnail": null,
      "UpdatedBy": null
   }
}
```
{: .colcontent}

## Numeric Input Question

`Question` | `CheckListQuestionID` | `QuestionType` | `PictureFlag`
-----|-----
How many lights need repair or replacement? | 3837 | 4 — Numeric Input | 0 — Adding an attachment is optional.

Only one step is left to complete the checklist. To summarize their visit, the technician specifies in `Answer` that one light needs repair or replacement and adds a picture to the response.

[<i class="fa fa-play" aria-hidden="true"></i>]({{base_path}}/swagger/ui/index?version=3#/CheckLists/POSTv3_checklists_CheckListResponse){:target="_blank"}{: .btn .btn--primary .btn--small} **Example request for a [Numeric Input]({{base_path}}/guides/checklists/types-of-checklist-questions/#numeric-input) question:**

```http
POST https://sb2api.servicechannel.com/v3/checklists/CheckListResponse HTTP/1.1
Authorization: Bearer {access_token}
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

----WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="CheckListResponseRequest"

{
   "CheckListQuestionID": 3837,
   "ProviderID": 2000090505,
   "TechnicianID": 371122,
   "Answer": 1,
   "EntityID": 180088351,
   "Attempt": 1
}
----WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="file"; filename="/C:/light-bulb.jpg"
Content-Type: image/jpeg

(data)
----WebKitFormBoundary7MA4YWxkTrZu0gW
```

**Example response**
{: .collapsible}

``` json
{
   "CheckListResponse": {
      "CheckListQuestionId": 3837,
      "CheckListQuestionOptionId": null,
      "ProviderId": 2000090505,
      "TechnicianId": 371122,
      "Id": 1932693,
      "Answer": "1",
      "FreeTextAnswer": null,
      "EntityId": 180088351,
      "EntityTypeId": 0,
      "Attempt": 1,
      "UpdatedDate": "2021-06-07T10:23:32.343",
      "UpdatedDateDto": "2021-06-07T10:23:32.343-04:00"
   },
   "CheckListResponsePicture": {
      "Id": 114062,
      "CheckListResponseId": 1932693,
      "DocumentName": "light-bulb.jpg",
      "DocumentPath": "#workorders/ab06d9f6-c87e-40ef-8842-94db43f33b05",
      "ThumbnailPath": "#workorders/37f2ab61-18bf-4b8f-a1f9-210cd540ca46",
      "HasThumbnail": true,
      "UpdatedBy": null
   }
}
```
{: .colcontent}

**Response code:** HTTP/1.1 201 Created

If the request succeeds, each checklist response you submitted is assigned a unique `Id`, and two objects — `CheckListResponse` and `CheckListResponsePicture` — are returned.

{% include collapsible.html %}